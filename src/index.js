const { GraphQLServer } = require('graphql-yoga')

const { prisma } = require('./generated/prisma-client')

const Query = require('./resolvers/Query')
const Mutation = require('./resolvers/Mutation')
const User = require('./resolvers/User')
const Link = require('./resolvers/Link')
const Subscription = require('./resolvers/Subscription')
const Vote = require('./resolvers/Vote')

const resolvers = {
    Query,
    Mutation,
    User,
    Link,
    Subscription,
    Vote,
}

// let links = [{
//     id: 'link-0',
//     description: 'Fullstack tutorial for GraphQL',
//     url: 'www.howtographql.com'
// }]

// let idCount = links.length

// const resolvers = {
//     Query: {
//         info: () => `This is the API of Hackernews Clone`,
//         feed: (root, args, context, info) => {
//             return context.prisma.links()
//         },
//     },
//     Mutation: {
//         post: (root, args, context, info) => {
//             // const link = {
//             //     id: `link-${idCount++}`,
//             //     description: args.description,
//             //     url: args.url,
//             // }
//             // links.push(link)
//             // return link
//             return context.prisma.createLink({
//                 url: args.url,
//                 description: args.description,
//             })
//         },
//         // deletePost: (parent, args) => {
//         //     links = links.filter( item => item.id != args.id)
//         // }
//     }
//     // Link: {
//     //     id: (parent) => parent.id,
//     //     description: (parent) => parent.description,
//     //     url: (parent) => parent.url
//     // }
// }

const server = new GraphQLServer({
    typeDefs: './src/schema.graphql',
    resolvers,
    context: request => {
        return {
            ...request,
            prisma,
        }
    },
})

server.start(() => console.log(`Server is running on http://localhost:4000`))